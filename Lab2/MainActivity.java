package com.example.a005135659.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /*
    Initializing variables
    A button, and two text views are needed, one of which is an input box
    The String variable is used later for conversion calculations
    */
    private Button convbutton;
    private EditText input;
    private TextView text;
    private String usd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        convbutton = findViewById(R.id.convertbutton);
        input = findViewById(R.id.EditText01);
        text = findViewById(R.id.Yen);

        convbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                usd = input.getText().toString();

                if(usd.equals("")) {
                    text.setText("Please enter an amount of USD.");
                }
                else {
                    Double valueUSD = Double.parseDouble(usd);

                    Double result = valueUSD * 112.57;

                    text.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));
                    input.setText("");
                }
            }


        });
    }
}
