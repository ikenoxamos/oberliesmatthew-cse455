package com.example.a005135659.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    /*
    Initializing variables
    A button, and two text views are needed, one of which is an input box
    The String variable is used later for conversion calculations
    */
    private Button convbutton;
    private EditText input;
    private TextView text;

    String url = "https://api.fixer.io/latest?base=USD";
    String usd, json = "";
    String rate = "";
    String line = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        convbutton = findViewById(R.id.convertbutton);
        input = findViewById(R.id.EditText01);
        text = findViewById(R.id.Yen);

        convbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {

                System.out.println("\nTesting 1 ... Before AsynchExecution\n");

                BackgroundTask task = new BackgroundTask();

                task.execute();

                System.out.println("\nTesting 2 ... After AsynchExecution\n");
            }
        });
    }


    private class BackgroundTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("The rate is: " + rate + "\n");

            double ratio = Double.parseDouble(rate);

            System.out.println("\nTesting JSON String Exchance Rate Inside AsynchTask: " + ratio);

            usd = input.getText().toString();

            if (usd.equals("")) {
                text.setText("Please enter an amount of USD.");
            } else {
                Double valueUSD = Double.parseDouble(usd);

                Double output = valueUSD * ratio;

                text.setText("$" + usd + " = " + "¥" + String.format("%.2f", output));
                input.setText("");
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection connection = (HttpURLConnection) web_url.openConnection();

                connection.setRequestMethod("GET");

                System.out.println("\nTesting ... Before connection method to URL\n");

                connection.connect();

                InputStream stream = connection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

                System.out.println("Connection Successful!\n");

                while (line != null) {
                    line = reader.readLine();
                    json += line;
                }

                System.out.println("Here is the JSON for comparison: " + json);

                JSONObject full_data = new JSONObject(json);
                JSONObject data = full_data.getJSONObject("rates");

                rate = data.get("JPY").toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                System.exit(1);
            }

            return null;
        }

    }
}
